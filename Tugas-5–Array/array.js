//Soal Nomer 1
function range(startNum, finishNum) {
    var outputarray;
    if (finishNum !== undefined) {
        outputarray = [];
        if (startNum < finishNum) {
            for (var index = startNum; index <= finishNum; index++) {
                outputarray.push(index);
            }
        } else if (startNum > finishNum) {
            for (var index = startNum; index >= finishNum; index--) {
                outputarray.push(index);
            }
        }
    } else {
        outputarray = -1;
    }
    return outputarray;
}

//Soal Nomer 2
function rangeWithStep(startNum, finishNum, step) {
    var outputarray = [];
    if (startNum < finishNum) {
        for (var index = startNum; index <= finishNum; index = index + step) {
            outputarray.push(index);
        }
    } else if (startNum > finishNum) {
        for (var index = startNum; index >= finishNum; index = index - step) {
            outputarray.push(index);
        }
    }
    return outputarray;
}

//Soal nomer 3
function sum(startNum, finishNum=0, step=1) {
    var outputsum = 0;
    if (startNum < finishNum) {
        for (var index = startNum; index <= finishNum; index = index + step) {
            outputsum = outputsum + index;
        }
    } else if (startNum > finishNum) {
        for (var index = startNum; index >= finishNum; index = index - step) {
            outputsum = outputsum + index;
        }
    }
    return outputsum;
}

//Soal Nomer 4
function dataHandling(inputArray) {
    for (let index = 0; index < inputArray.length; index++) {
        console.log("Nomor ID: " + inputArray[index][0]);
        console.log("Nama Lengkap: " + inputArray[index][1]);
        console.log("TTL: " + inputArray[index][2] + " " +inputArray[index][3]);
        console.log("Hobi: " + inputArray[index][4]);
        console.log(" ");
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

//console.log(" ");
//console.log("Soal Nomer 4");
//dataHandling(input);

//Soal Nomer 5
function balikKata(inputstring) {
    var outputString = "";
    for (let index = inputstring.length - 1; index >= 0; index--) {
        outputString = outputString + inputstring[index];
    }
    return outputString;
}

//Soal Nomer 6
function dataHandling2(inputArray) {
    inputArray.splice(4, 1, "Pria", "SMA Internasional Metro");
    inputArray.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    console.log(inputArray);
    var arrayDate = inputArray[3].split("/");
    switch (parseInt(arrayDate[1])) {
        case 01:
            console.log("January");
            break;
        case 02:
            console.log("February");
            break;
        case 03:
            console.log("March");
            break;
        case 04:
            console.log("April");
            break;
        case 05:
            console.log("May");
            break;
        case 06:
            console.log("June");
            break;
        case 07:
            console.log("July");
            break;
        case 08:
            console.log("August");
            break;
        case 09:
            console.log("September");
            break;
        case 10:
            console.log("October");
            break;
        case 11:
            console.log("November");
            break;
        case 12:
            console.log("December");
            break;
    }
    console.log(arrayDate);
    console.log(arrayDate.join("-"));
    arrayDate.sort(function (value1, value2) {
        return value2 - value1
    });
    console.log(arrayDate);
    console.log(inputArray[1].slice(0,15));
}

var input2 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input2);