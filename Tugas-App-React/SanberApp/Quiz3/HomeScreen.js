import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Button,
} from 'react-native';
import { block } from 'react-native-reanimated';

import data from './data.json';

const DEVICE = Dimensions.get('window');

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    var updatePrice = this.updatePrice.bind(this);
    this.state = {
      searchText: "",
      totalPrice: 0,
    };
  }

  currencyFormat(num) {
    return "Rp " + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  }

  updatePrice(price) {
    this.setState({ totalPrice: this.state.totalPrice + parseInt(price)});
  }

  render() {
    var updatePrice = this.updatePrice;
    return (
      <View style={styles.container}>
        <View
          style={{
            minHeight: 50,
            width: DEVICE.width * 0.88 + 20,
            marginVertical: 8,
          }}
        >
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <Text>
              Hai,{"\n"}
              {/* //? #Soal 1 Tambahan @done*/}
              <Text style={styles.headerText}>
                {this.props.route.params.key}
              </Text>
            </Text>

            {/* //? #Soal Bonus @done */}
            <Text style={{ textAlign: "right" }}>
              Total Harga{"\n"}
              <Text style={styles.headerText}>
                {this.currencyFormat(this.state.totalPrice)}
              </Text>
            </Text>
          </View>
          <View></View>
          <TextInput
            style={{ backgroundColor: "white", marginTop: 8 }}
            placeholder="Cari barang.."
            onChangeText={(searchText) => this.setState({ searchText })}
          />
        </View>

        {/* 
        //? #Soal No 2 (15 poin) @Done
        */}
        <FlatList
          data={data.produk}
          renderItem={(data) => (
            <ListItem data={data} updatePrice={updatePrice.bind(this)} />
          )}
          numColumns={2}
        />
      </View>
    );
  }
}

class ListItem extends React.Component {
  currencyFormat(num) {
    return "Rp " + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  }

  //? #Soal No 3 @done
  render() {
    const data = this.props.data.item;
    var updatePrice = this.props.updatePrice;
    return (
      <View style={styles.itemContainer}>
        <Image
          source={{ uri: data.gambaruri }}
          style={styles.itemImage}
          resizeMode="contain"
        />
        <Text numberOfLines={2} ellipsizeMode="tail" style={styles.itemName}>
          {data.nama}
        </Text>
        <Text style={styles.itemPrice}>
          {this.currencyFormat(Number(data.harga))}
        </Text>
        <Text style={styles.itemStock}>Sisa stok: {data.stock - 1}</Text>
        <Button
          title="BELI"
          color="blue"
          onPress={() => {updatePrice(Number(data.harga));}}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  headerText: {
    fontSize: 18,
    fontWeight: "bold",
  },

  //? Lanjutkan styling di sini
  itemContainer: {
    width: DEVICE.width * 0.44,
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "#FFFFFF",
    marginHorizontal: 5,
    marginVertical: 5,
    padding: 5,
  },
  itemImage: {
    width: '100%',
    height: 100
  },
  itemName: {
    fontWeight: "bold",
  },
  itemPrice: {
    color: "blue",
    fontSize: 14
  },
  itemStock: {
    color: "green"
  },
  Button: {
  },
  buttonText: {},
});
