import React from "react";
import { View, Text, StyleSheet, Button, Alert } from "react-native";
import { AuthContext } from "./context";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginVertical: 10,
    borderRadius: 5,
  },
});

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Login = ({ navigation }) => {
    const {signIn} = React.useContext(AuthContext);
  return (
    <ScreenContainer>
      <Text>This Is Login Screen</Text>
      <Button title="Login" onPress={() => signIn()} />
    </ScreenContainer>
  );
};
