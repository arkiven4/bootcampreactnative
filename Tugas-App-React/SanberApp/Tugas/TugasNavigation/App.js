import React, { Component, useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  TextInput,
  Button,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import { getStatusBarHeight } from "react-native-status-bar-height";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";

import {Login} from "./LoginScreen";
import {Skill} from "./SkillScreen";
import { Project } from "./ProjectScreen";
import {Add} from "./AddScreen";
import {About} from "./AboutScreen";
import {AuthContext} from "./context";

const LoginStack = createStackNavigator();
const SkillStack = createStackNavigator();
const ProjectStack = createStackNavigator();
const AddStack = createStackNavigator();
const AboutStack = createStackNavigator();

const Tabs = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const SkillStackScreen = () => {
  return (
    <SkillStack.Navigator>
      <SkillStack.Screen name="Skill" component={Skill} />
    </SkillStack.Navigator>
  );
};

const ProjectStackScreen = () => {
  return (
    <ProjectStack.Navigator>
      <ProjectStack.Screen name="Project" component={Project} />
    </ProjectStack.Navigator>
  );
};

const AddStackScreen = () => {
  return (
    <AddStack.Navigator>
      <AddStack.Screen name="Add" component={Add} />
    </AddStack.Navigator>
  );
};

const TabsScreen = () => {
  return (
    <Tabs.Navigator>
      <Tabs.Screen name="Skill" component={SkillStackScreen} />
      <Tabs.Screen name="Project" component={ProjectStackScreen} />
      <Tabs.Screen name="Add" component={AddStackScreen} />
    </Tabs.Navigator>
  );
};

const AboutScreen = () => {
  return (
    <AboutStack.Navigator>
      <AboutStack.Screen name="About" component={About} />
    </AboutStack.Navigator>
  );
};

export default () => {
    const [userToken, setUserToken] = useState(null);
    const authContext = React.useMemo(() => {
      return {
        signIn: () => {
          setUserToken("Jaja")
        }
      }
    },[])
    return (
      <AuthContext.Provider value={authContext}>
        <NavigationContainer>
          {userToken ? (
            <Drawer.Navigator>
              <Drawer.Screen name="Home" component={TabsScreen} />
              <Drawer.Screen name="About" component={AboutScreen} />
            </Drawer.Navigator>
          ) : (
            <LoginStack.Navigator>
              <LoginStack.Screen name="Login" component={Login} />
            </LoginStack.Navigator>
          )}
        </NavigationContainer>
      </AuthContext.Provider>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
