import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  TextInput,
  Button,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import { getStatusBarHeight } from "react-native-status-bar-height";

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      username: "Input Username",
      password: "Input Password",
    };
  }

  render() {
    return (
      <View View style={styles.container}>
        <Image source={require("./images/logo.png")} style={styles.LogoLogin} />
        <View style={styles.LoginFormContainer}>
          <Text style={styles.LoginText}>Login</Text>
          <View style={styles.InputTextContainer}>
            <Text style={{ textAlign: "left", alignSelf: "stretch" }}>
              Username/Email
            </Text>
            <TextInput
              style={styles.TextInput}
              onChangeText={(text) => this.setState({ username: text })}
              value={this.state.username}
            ></TextInput>
          </View>
          <View style={styles.InputTextContainer}>
            <Text style={{ textAlign: "left", alignSelf: "stretch" }}>
              Password
            </Text>
            <TextInput
              style={styles.TextInput}
              onChangeText={(text) => this.setState({ password: text })}
              value={this.state.password}
            ></TextInput>
          </View>
          <TouchableOpacity
            style={[styles.RoundButton, { backgroundColor: "#00BCD4" }]}
          >
            <Text style={{ color: "#fFff" }}>Login</Text>
          </TouchableOpacity>
          <Text>Atau</Text>
          <TouchableOpacity
            style={[styles.RoundButton, { backgroundColor: "#003366" }]}
          >
            <Text style={{ color: "#fFff" }}>Daftar?</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: getStatusBarHeight(),
  },
  LogoLogin: {
    paddingTop: 73,
  },
  LoginFormContainer: {
    alignItems: "center",
  },
  LoginText: {
    paddingVertical: 30,
    fontSize: 24,
  },
  TextInput: {
    height: 40,
    width: "100%",
    borderColor: "gray",
    borderWidth: 1,
    paddingLeft: 5,
  },
  InputTextContainer: {
    width: "80%",
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 20,
  },
  RoundButton: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 10,
    borderColor: "#fff",
    marginVertical: 10,
  },
});
