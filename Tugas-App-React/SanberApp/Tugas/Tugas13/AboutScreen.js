import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  TextInput,
  Button,
  Dimensions,
  ScrollView,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import { getStatusBarHeight } from "react-native-status-bar-height";
//import VideoItem from './component/videoitem.js';

const { height, width } = Dimensions.get("window");
export default class App extends Component {
  constructor() {
    super();
    this.state = {
      username: "Input Username",
      password: "Input Password",
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{ paddingBottom: 10 }}>
          <View style={styles.TentangContainer}>
            <Text style={styles.TentangH1}>Tentang Saya</Text>
            <Image
              source={{
                uri: "https://randomuser.me/api/portraits/women/67.jpg",
              }}
              style={{
                height: width * 0.4,
                width: width * 0.4,
                borderRadius: 100,
              }}
            ></Image>
            <Text style={styles.TentangNameText}>Mukhlis Hanafi</Text>
            <Text style={styles.TentangJobText}>React Native Developer</Text>
          </View>
          <View style={styles.PortofolioContainer}>
            <Text>Portofolio</Text>
            <View
              style={{
                borderBottomColor: "black",
                borderBottomWidth: 1,
              }}
            />
            <View style={styles.PortofolioIconConatiner}>
              <TouchableOpacity style={styles.tabItems}>
                <Icon name="gitlab" size={35} style={{ color: "#3EC6FF" }} />
                <Text style={styles.TabTitle}>@mukhlish</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.tabItems}>
                <Icon name="github" size={35} style={{ color: "#3EC6FF" }} />
                <Text style={styles.TabTitle}>@mukhlis-h</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.PortofolioContainer}>
            <Text>Hubungi Saya</Text>
            <View
              style={{
                borderBottomColor: "black",
                borderBottomWidth: 1,
              }}
            />
            <View style={styles.HubungisayaIconConatiner}>
              <TouchableOpacity style={styles.tabItems2}>
                <Icon
                  name="facebook-square"
                  size={35}
                  style={{ color: "#3EC6FF" }}
                />
                <Text style={styles.TabTitle2}>mukhlis.hanafi</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.tabItems2}>
                <Icon name="instagram" size={35} style={{ color: "#3EC6FF" }} />
                <Text style={styles.TabTitle2}>@mukhlis_hanafi</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.tabItems2}>
                <Icon name="twitter" size={35} style={{ color: "#3EC6FF" }} />
                <Text style={styles.TabTitle2}>@mukhlis_hanafi</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ marginBottom: 10 }}></View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: getStatusBarHeight(),
  },
  TentangContainer: {
    alignItems: "center",
  },
  TentangH1: {
    fontWeight: "bold",
    fontSize: 30,
    marginTop: height * 0.04,
    marginBottom: 15,
  },
  TentangNameText: {
    fontWeight: "bold",
    fontSize: 19,
    marginTop: 10,
    marginBottom: 5,
  },
  TentangJobText: {
    fontWeight: "bold",
    fontSize: 15,
    marginBottom: 5,
    color: "#3EC6FF",
  },
  PortofolioContainer: {
    marginHorizontal: 15,
    backgroundColor: "#EFEFEF",
    borderRadius: 20,
    padding: 8,
    marginVertical: 5,
  },
  PortofolioIconConatiner: {
    margin: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
  },
  HubungisayaIconConatiner: {
    margin: 10,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-around",
  },
  tabItems: {
    alignItems: "center",
    justifyContent: "center",
  },
  tabItems2: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginVertical: 10,
  },
  TabTitle: {
    marginTop: 5,
  },
  TabTitle2: {
    marginLeft: 10,
  },
});
