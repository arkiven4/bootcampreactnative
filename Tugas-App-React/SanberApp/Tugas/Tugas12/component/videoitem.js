import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class VideoItem extends Component {
    render(){
        let video = this.props.video;
        return (
            <View style={styles.container}>
                <Image source={{uri: video.snippet.thumbnails.medium.url}} style={{height: 200}} />
                <View style={styles.descContainer}>
                    <Image source={{uri: 'https://randomuser.me/api/portraits/women/61.jpg'}} style={{height: 50, width: 50, borderRadius: 30}} />
                    <View style={styles.videoDetails}>
                        <Text numberOfLines={2} style={styles.videoTitle}>{video.snippet.title}</Text>
                        <Text style={styles.videoDesc}>{video.snippet.channelTitle+" · "+ nFormatter(video.statistics.viewCount, 1)} Views</Text>
                        <Text style={styles.videoDesc}>{timeSince(video.snippet.publishedAt)}</Text>
                    </View>
                    <TouchableOpacity><Icon style={styles.NavItems} name="more-vert" size={20}/></TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 15
    },
    descContainer : {
        flexDirection: 'row',
        paddingTop: 15,
    },
    videoTitle: {
        fontSize: 14,
    },
    videoDesc: {
        fontSize: 13,
        color: 'grey',
        paddingTop: 3
    },
    videoDetails: {
        paddingHorizontal: 15,
        flex: 1
    }
})

function nFormatter(num, digits) {
   var si = [
    { value: 1, symbol: "" },
    { value: 1E3, symbol: "k" },
    { value: 1E6, symbol: "M" },
    { value: 1E9, symbol: "G" },
    { value: 1E12, symbol: "T" },
    { value: 1E15, symbol: "P" },
    { value: 1E18, symbol: "E" }
  ];
  var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  var i;
  for (i = si.length - 1; i > 0; i--) {
    if (num >= si[i].value) {
      break;
    }
  }
  return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
}

const currentTimeStamp = new Date().getTime();

function timeSince(timeStamp) {
    var previousdate = new Date(timeStamp);
    var current = new Date();  
    var minutes = 60 * 1000;
    var hours = minutes * 60;
    var days = hours * 24;
    var months = days * 30;
    var years = days * 365;

    var elapsed = current - previousdate;

    if (elapsed < minutes) {
         return Math.round(elapsed/1000) + ' seconds ago';   
    }

    else if (elapsed < hours) {
         return Math.round(elapsed/minutes) + ' minutes ago';   
    }

    else if (elapsed < days ) {
         return Math.round(elapsed/hours ) + ' hours ago';   
    }

    else if (elapsed < months) {
         return Math.round(elapsed/days) + ' days ago';   
    }

    else if (elapsed < years) {
         return Math.round(elapsed/months) + ' months ago';   
    }

    else {
         return Math.round(elapsed/years) + ' years ago';   
    }
}