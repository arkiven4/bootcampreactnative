console.log("Soal Nomer 1");
console.log("LOOPING PERTAMA");
var index1 = 2;
while (index1 <= 20) {
    if (index1 % 2 == 0) {
        console.log(index1 + " - I love coding");
    }
    index1++;
}
console.log("LOOPING KEDUA");
var index2 = 20;
while (index2 > 0) {
    if (index2 % 2 == 0) {
        console.log(index2 + " - I love coding");
    }
    index2--;
}

console.log("");
console.log("Soal Nomer 2");
for (let index = 1; index <= 20; index++) {
    if (index % 3 == 0 && index % 2 != 0) {
        console.log(index + " - I love coding");
    } else if (index % 2 == 0) {
        console.log(index + " - Berkualitas");
    } else if (index % 2 != 0) {
        console.log(index + " - Santai");
    }
}

console.log("");
console.log("Soal Nomer 3");
var strout = "";
for (let i = 0; i < 4; i++) {
    strout = "";
    for (let j = 0; j < 8; j++) {
        strout = strout + "#";
    }
    console.log(strout);
}

console.log("");
console.log("Soal Nomer 4");
var strout2 = "";
for (let i = 1; i < 8; i++) {
    strout2 = "";
    for (let j = 0; j < i; j++) {
        strout2 = strout2 + "#";
    }
    console.log(strout2);
}

console.log("");
console.log("Soal Nomer 5");
var strout3 = "";
for (let i = 0; i < 8; i++) {
    strout3 = "";
    if (i % 2 == 0) { //Baris Genap
        for (let j = 0; j < 4; j++) {
            strout3 = strout3 + " #";
        }
    } else if (i % 2 != 0) {
        for (let j = 0; j < 4; j++) {
            strout3 = strout3 + "# ";
        }
    }
    console.log(strout3);
}