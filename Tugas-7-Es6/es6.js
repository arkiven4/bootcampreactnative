//Soal nomer 1
const golden = goldenFunction = () => {
    console.log("this is golden!!")
}
golden()

//soal Nomer 2
const newFunction = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: () => {
            console.log(`${firstName} ${lastName}`)
            return
        }
    }
}
newFunction("William", "Imoh").fullName()

//Soal Nomer 3
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const {
    firstName,
    lastName,
    destination,
    occupation
} = newObject;

console.log(firstName, lastName, destination, occupation);

//Soal Nomer 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east];
console.log(combined)

//Soal nomer 5
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;
console.log(before)