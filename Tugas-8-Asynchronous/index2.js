var readBooksPromise = require('./promise.js')

var books = [{
        name: 'LOTR',
        timeSpent: 3000
    },
    {
        name: 'Fidas',
        timeSpent: 2000
    },
    {
        name: 'Kalkulus',
        timeSpent: 4000
    }
]

var nowIndexbuku = 0;
function Readbook(sisaWaktu) {
    if (nowIndexbuku >= books.length || sisaWaktu == 0) {
        console.log("Done");
    } else {
        readBooksPromise(sisaWaktu, books[nowIndexbuku]).then((result) => {
            nowIndexbuku++;
            Readbook(result);
        }).catch((err) => {
            console.log(err);
        });
    }
}

Readbook(10000);