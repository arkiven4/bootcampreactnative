var readBooks = require('./callback.js')

var books = [{
        name: 'LOTR',
        timeSpent: 3000
    },
    {
        name: 'Fidas',
        timeSpent: 2000
    },
    {
        name: 'Kalkulus',
        timeSpent: 4000
    }
]

var nowIndexbuku = 0;
readBooks(10000, books[nowIndexbuku], function (sisaWaktu) {
    afterReadbook(sisaWaktu);
})

function afterReadbook(sisaWaktu) {
    nowIndexbuku++;
    if (nowIndexbuku >= books.length || sisaWaktu == 0) {
        console.log("Done");
    } else {
        readBooks(sisaWaktu, books[nowIndexbuku], afterReadbook);
    }
}