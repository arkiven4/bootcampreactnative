//Soal Nomer 1
function arrayToObject(inputArray) {
    var now = new Date();
    var thisYear = now.getFullYear();
    var outputObject = {};
    var nowAge = 0;
    if (inputArray == []) {
        return "";
    } else {
        for (let index = 0; index < inputArray.length; index++) {
            if (inputArray[index][3] > thisYear || inputArray[index][3] == undefined) {
                nowAge = "Invalid Birth Year";
            } else {
                nowAge = thisYear - inputArray[index][3];
            }
            outputObject[(index + 1) + '. ' + inputArray[index][0]] = {
                firstName: "Bruce",
                lastName: "Banner",
                gender: "male",
                age: nowAge
            }
        }
        return outputObject;
    }
}

var input = [
    ["Abduh", "Muhamad", "male", 2023],
    ["Ahmad", "Taufik", "male", 1985]
];
console.log(arrayToObject(input));

//Soal Nomer 2
function shoppingTime(memberId, money) {
    var nowMoney = money;
    var SaleData = {};
    var purchahedItem = [];
    SaleData['Sepatu Stacattu'] = 1500000;
    SaleData['Baju Zoro'] = 500000;
    SaleData['Baju H&N'] = 250000;
    SaleData['Sweater Uniklooh'] = 175000;
    SaleData['Casing Handphone'] = 50000;
    if (memberId == "" || memberId == undefined) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup";
    } else {
        for (const [key, value] of Object.entries(SaleData)) {
            if (value <= nowMoney) {
                nowMoney = nowMoney - value;
                purchahedItem.push(key);
            }
        }
        return {
            memberId: memberId,
            money: money,
            listPurchased: purchahedItem,
            changeMoney: nowMoney
        }
    }
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));

[
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B']
]

//Soal nomer 3
function naikAngkot(listPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var nowObject = {};
    var returnArray = [];
    var nowBayar = 0;
    if (listPenumpang == []) {
        return [];
    } else {
        for (let index = 0; index < listPenumpang.length; index++) {
            nowBayar = 0;
            for (let i = rute.indexOf(listPenumpang[index][1]); i < rute.indexOf(listPenumpang[index][2]); i++) {
                nowBayar += 2000;
            }
            nowObject = {
                penumpang: listPenumpang[index][0],
                naikDari: listPenumpang[index][1],
                tujuan: listPenumpang[index][2],
                bayar: nowBayar
            }
            returnArray.push(nowObject);
        }
        return returnArray;
    }
}
console.log(naikAngkot([
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B']
]));