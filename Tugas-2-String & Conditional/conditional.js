console.log("Soal Nomer 1");
var nama = "John"
var peran = ""

if (nama == '' && peran == '') {
    console.log("Nama harus diisi!");
} else if (nama == 'John' && peran == '') {
    console.log("Halo John, Pilih peranmu untuk memulai game!");
} else if (nama == 'Jane' && peran == 'Penyihir') {
    console.log("Selamat datang di Dunia Werewolf, Jane");
    console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!");
} else if (nama == 'Jenita' && peran == 'Guard') {
    console.log("Selamat datang di Dunia Werewolf, Jenita");
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.");
} else if (nama == 'Junaedi' && peran == 'Werewolf') {
    console.log("Selamat datang di Dunia Werewolf, Junaedi");
    console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!");
}

console.log("Soal Nomer 2");
var hari = 21;
var bulan = 5;
var tahun = 1945;

switch (bulan) {
        case 1: bulan = "January";
        break;
        case 2: bulan = "February";
        break;
        case 3: bulan = "March";
        break;
        case 4: bulan = "April";
        break;
        case 5: bulan = "May";
        break;
        case 6: bulan = "June";
        break;
        case 7: bulan = "July";
        break;
        case 8: bulan = "August";
        break;
        case 9: bulan = "September";
        break;
        case 10: bulan = "October";
        break;
        case 11: bulan = "November";
        break;
        case 12: bulan = "December";
        break;
}

console.log("Ouput Tanggal : " + hari + " " + bulan + " " + tahun);